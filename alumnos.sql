﻿
DROP DATABASE IF EXISTS colegio; 
  
CREATE DATABASE colegio;
USE colegio;

-- crear tablas de la base de datos

CREATE TABLE profesor(
    
      dni varchar(10),
      nombre varchar (100),
      direccion varchar (100),
      telefono varchar (15),
  PRIMARY KEY (dni)

);

CREATE TABLE alumunos (
        expediente int AUTO_INCREMENT,
         nombre varchar(100),
        apellidos varchar(100),
         fecha_nacimiento date,
        PRIMARY KEY (expediente)
  );


CREATE TABLE modulos (
         codigo int AUTO_INCREMENT,
         nombre varchar(100),
          PRIMARY KEY (codigo)
  );
CREATE TABLE imparte (
       
         codigomodu int,
        dniprofesor varchar(10),
        PRIMARY KEY (codigomodu,dniprofesor),
        CONSTRAINT fkimpartemodulo
        FOREIGN KEY ( codigomodu) REFERENCES modulos(codigo),
        CONSTRAINT fkimparteprofesor
        FOREIGN KEY ( dniprofesor) REFERENCES profesor(dni)
  );
CREATE TABLE cursa (
        
         expedientealu int,
         codigomodu1 int,
        PRIMARY KEY (expedientealu,codigomodu1),
 CONSTRAINT fkcursaalumno
        FOREIGN KEY ( expedientealu) REFERENCES alumunos(expediente),
        CONSTRAINT fkcursamodulo
        FOREIGN KEY ( codigomodu1) REFERENCES modulos(codigo)
  );
CREATE TABLE delegado (
        expedientedelegado int,
         expedienteestudiante int,
        PRIMARY KEY (expedientedelegado,expedienteestudiante),
        UNIQUE KEY (expedienteestudiante),
 CONSTRAINT fkdelgadoalumno
        FOREIGN KEY (expedientedelegado) REFERENCES alumunos(expediente),
        CONSTRAINT fkdelegadoalumno1
        FOREIGN KEY (expedienteestudiante) REFERENCES alumunos(expediente)


  );